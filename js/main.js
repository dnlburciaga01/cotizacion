const btnCalcular = document.getElementById('btnCalcular');

btnCalcular.addEventListener("click", function(){

   //obtener los valores de los inputs text
   let valorAuto = document.getElementById('valorAuto').value;
   let pIncial = document.getElementById('porPagoIncial').value;
   let plazos = document.getElementById('plazos').value;

   //hacer calculos
   let pagoIncial = valorAuto * (pIncial/100);
   let totalFin = valorAuto-pagoIncial;
   let pagoMensual = totalFin/plazos;


   //mostrar los resultados

   document.getElementById('pagoIncial').value = pagoIncial;
   document.getElementById('totalFin').value = totalFin;
   document.getElementById('pagoMensual').value = pagoMensual.toFixed(2);

});

//Codificación del botón Registrar
function agregarRegistro() {
   let valorAuto = document.getElementById('valorAuto').value;
   let pIncial = document.getElementById('porPagoIncial').value;
   let plazos = document.getElementById('plazos').value;
   let pagoIncial = document.getElementById('pagoIncial').value;
   let totalFin = document.getElementById('totalFin').value;
   let pagoMensual = document.getElementById('pagoMensual').value;

    const tabla = document.getElementById("registros");
    const newRow = `
    <tr>
      <td>${valorAuto}</td>
      <td>${pIncial}</td>
      <td>${plazos}</td>
      <td>${pagoIncial}</td>
      <td>${totalFin}</td>
      <td>${pagoMensual}</td>
    </tr>
  `;
  
    tabla.innerHTML += newRow;

    let tGeneral = 0;
    const filas = tabla.getElementsByTagName("tr");
  
    for (let i = 1; i < filas.length; i++) {
      const tPagar = parseFloat(filas[i].getElementsByTagName("td")[5].textContent);
      tGeneral += tPagar;
    }
  
    document.getElementById("total").textContent = "$"+tGeneral.toFixed(2);
  }

//Botón Borrar
function eliminarRegistro(){
    const tabla = document.getElementById("registros");
    let numRows = tabla.rows.length;
    for (let i = numRows - 1; i > 0; i--) {
        tabla.deleteRow(i);
    }
    let totalGeneral=0;
    document.getElementById("total").textContent = totalGeneral;
  }
